FROM node:20.13.1

WORKDIR /usr/src/app

COPY package.json package-lock.json /usr/src/app/

RUN npm install

COPY . /usr/src/app/
ENV PORT=3001
EXPOSE 3001

CMD npm run start
